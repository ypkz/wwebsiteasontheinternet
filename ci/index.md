---
layout: default
title: Continuous Integration - 0X1A
---

<div class="row">
    <div class="col-lg-12 section">
	<h1 class="section-heading"><a href="{{ site.url }}/ci/motor">Motor</a></h1>
	<a href="{{ site.url }}/ci/motor"><img style="float:left; padding-right:10px" src="{{ site.url }}/img/motor-build.png"><img style="float:left" src="{{ site.url }}/img/motor-coverage.png"></a></br></br>
	<h1 class="section-heading"><a href="{{ site.url }}/ci/yabs">Yabs</a></h1>
	<a href="{{ site.url }}/ci/yabs"><img style="float:left; padding-right:10px" src="{{ site.url }}/img/yabs-build.png"><img style="float:left" src="{{ site.url }}/img/yabs-coverage.png"></a></br></br>
    </div>
</div>
